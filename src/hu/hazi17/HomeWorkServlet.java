package hu.hazi17;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SecondServlet
 */
@WebServlet("/HomeWorkServlet")
public class HomeWorkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HomeWorkServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Enumeration params = request.getParameterNames();

		out.print("<html>\r\n" + "   <head>\r\n"
				+ "   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n"
				+ "      <title>simple login</title>\r\n" + "   </head>\r\n" + "   <body>\r\n"
				+ "      <center><h3>Simple Login</h3></center>      \r\n"
				+ "	 <table border = \"1\" cellpadding=\"2\" cellspacing=\"2\" style=\"width:100%\">\r\n"
				+ "	 <tr align=\"center\" >\r\n" + "	 <hr />\r\n"
				+ "	<th colspan=\"3\" bgcolor=\"#0000ff\"> <font color=\"white\">Login Data </font></th>\r\n"
				+ "	 </tr>\r\n" + "	 <form method=\"GET\" action=\"HomeWorkServlet\">\r\n" + "	 <tr>\r\n"
				+ "            <td>username</td>\r\n"
				+ "            <td><input type=\"text\" name=\"username\" ></input></td>			\r\n"
				+ "         </tr>\r\n" + "         <tr>\r\n" + "            <td>vezeteknev</td>\r\n"
				+ "            <td><input type=\"text\" name=\"vezetekNev\" ></input></td>\r\n" + "         </tr>\r\n"
				+ "         <tr>\r\n" + "            <td>keresztnev</td>\r\n"
				+ "            <td><input type=\"text\" name=\"keresztNev\" ></input></td>\r\n" + "         </tr>\r\n"
				+ "		 <tr>\r\n" + "            <td>email</td>\r\n"
				+ "            <td><input type=\"text\" name=\"email\" ></input></td>\r\n"
				+ "         </tr>		 \r\n" + "		 <tr>\r\n"
				+ "            <td colspan=\"2\"><center><input type=\"submit\" value=\"OK\"></center></td> \r\n"
				+ "         </tr>\r\n" + "		 </form>\r\n" + "      </table>\r\n" + "	  <hr />\r\n"
				+ "   </body>\r\n" + "</html>");

		while (params.hasMoreElements())

		{
			String paramName = (String) params.nextElement();
			// param értéke akár több is lehet ezért tömbbe tároljuk
			String paramValues[] = request.getParameterValues(paramName);

			if (paramValues.length == 1) {
				out.println(paramName + " : " + paramValues[0] + "<br>");
			} else {
				// pl http://localhost:8080/FirstServlet/?nev=3&param2=ezzel&param2=azzal
				// http://10.3.44.1:8080/FirstServlet/?nev=3&param2=ezzel&param2=azzal
				out.println(paramName + " : ");
				for (int i = 0; i < paramValues.length; i++) {
					if (i > 0) {
						out.print(", ");
					}
					out.print(paramValues[i]);
				}

			}

		}
		out.println("</body>");
		out.println("</head>");
		out.println("</html>");

	}

}
