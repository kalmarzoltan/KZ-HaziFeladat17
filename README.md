**Házi feladat:**
1. Készítsetek egy Servlet -et, amely képes fogadni egy regisztrációs űrlapot.
Az űrlap elemei:
* username
* vezeteknev
* keresztnev
* email
2. Az űrlapot html-ben készítsétek el és egy gomb nyomásra küldődjön el a servletnek.
A Servlet pedig írja ki a beküldött adatokat.

**output:**
<center>![picture](output.png)</center>